This is a simple Javascript example to make a todo list. User can enter the todo list and they are automatically displayed with the option to edit, delete or mark as completed for each todo task. 

The png files shows how the interface looks. 


The focus is mainly on JavaScript.