var todo = {
	todoList:[],

	// Add todo objects
	addTodoItems: function(todoText){
		var todoItem = {
			todo: todoText,
			status: false
		};
		this.todoList.push(todoItem);
		display.displayTodoItems();
	}, 


	//toggle completed status , strike the list if completed
	toggleStatus:function(positionOfItem){
		var itemToToggle = this.todoList[positionOfItem];
		itemToToggle.status = !itemToToggle.status;
		display.displayTodoItems();

	},

	//toggle the status of all items
	toggleAllStatus:function(){
		var countOfCompletedList = 0;
		this.todoList.forEach(function(eachItem){
			if (eachItem.status === true){
				countOfCompletedList+=1;
			}
		});
		if (countOfCompletedList === this.todoList.length){
			this.todoList.forEach(function(eachItem){
				eachItem.status = false;
			});
		}
		else {
			this.todoList.forEach(function(eachItem){
				eachItem.status = true;
			});
		}
		display.displayTodoItems();
	},


	//delete todo item
	deleteTodoItems: function(positionOfItem){
		this.todoList.splice(positionOfItem, 1);
		display.displayTodoItems();
	},

	//edit todo item
	editTodoItems: function(positionOfItem, newTodo){
		this.todoList[positionOfItem].todo = newTodo;
		display.displayTodoItems();
	}


};


//handlers to handle events
var handlers = {
	addHandler: function(){
		var addTodoInputText = document.getElementById('addTodoInputText');
		var warningElement = document.getElementById('warnings');
		var warningText = '';
		if (addTodoInputText.value === ''){
			
			alert('Add valid todos!');
			warningElement.innerHTML = warningText;
			return;

		}
		todo.addTodoItems(addTodoInputText.value);
		addTodoInputText.value = '';
		warningElement.innerText ='';

	},

	deleteHandler: function(positionOfItem){
		todo.deleteTodoItems(positionOfItem);
	},

	editHandler: function(positionOfItem, newTodoText){
		if (newTodoText === ''){
			alert('Please enter a valid todo text!');
		}
		else{
			todo.editTodoItems(positionOfItem, newTodoText);
		}
	},

	toggleStatusHandler: function(positionOfItem){
		todo.toggleStatus(positionOfItem);
	},

	toggleAllHandler: function(){
		todo.toggleAllStatus();
	}
};


// display views
var display = {
	displayTodoItems : function(){
		var todoOl = document.querySelector('Ol');
		todoOl.innerHTML = '';
		todo.todoList.forEach(function(eachItem, indexOfItem){
			var todoli = document.createElement('li');
			todoli.id= indexOfItem;
			//todoli.innerText = eachItem.todo;
			var todoSpan = this.returnTodoSpan();
			todoSpan.innerText=eachItem.todo;
			todoli.appendChild(todoSpan);
			todoli.appendChild(this.displayTodoDeleteButton());
			todoli.appendChild(this.displayTodoEditButton());
			todoli.appendChild(this.displayToggleButton());
			todoOl.appendChild(todoli);
			if (eachItem.status === true){
				var strikedTodoText = todoSpan.innerText.strike();
				todoSpan.innerHTML = strikedTodoText;
			}
		},this);

	},

	returnTodoSpan:function(){
		var todoSpan = document.createElement('span');
		todoSpan.className = 'todoText';
		return todoSpan;
	},

	displayTodoDeleteButton: function(){
		var todoDeleteButton = document.createElement('button');
		todoDeleteButton.className = 'todoDeleteButton';
		todoDeleteButton.innerText = 'Delete';
		return todoDeleteButton;
	},

	displayTodoEditButton: function(){
		var todoEditButton = document.createElement('button');
		todoEditButton.className = 'todoEditButton';
		todoEditButton.innerText = 'Edit';
		return todoEditButton;
	},

	displayToggleButton: function(){
		var toggleButton = document.createElement('button');
		toggleButton.className = 'toggleButton';
		toggleButton.innerText = 'Mark Un/Completed';
		return toggleButton;
	},

	

};
		
var todoOl = document.querySelector('ol');
todoOl.addEventListener('click', function(event){
	var clickedButton = event.target;
	if(clickedButton.className === 'todoDeleteButton'){
		var todoItemToDelete = clickedButton.parentNode.id;
		handlers.deleteHandler(parseInt(todoItemToDelete));
	}
	else if(clickedButton.className === 'todoEditButton'){
		// if edit button then hide button and display input field
		clickedButton.style.display = 'none';
		var liOfCurrentItem = this.children[parseInt(clickedButton.parentNode.id)];
		var idOfCurrentItem = parseInt(clickedButton.parentNode.id);
		var editInputText = document.createElement('input');
		editInputText.className = 'editInput';

		editInputText.value = liOfCurrentItem.firstChild.innerText;
		var newTodoText = editInputText.value;
		clickedButton.parentNode.appendChild(editInputText);
		editInputText.addEventListener('keyup', function(event){
			
			if (event.keyCode == 13){
				handlers.editHandler(idOfCurrentItem, editInputText.value);
			}
		});

	}

	else if(clickedButton.className === 'toggleButton'){
		var positionOfItem = parseInt(clickedButton.parentNode.id);
		handlers.toggleStatusHandler(positionOfItem);
	}
	

});
var addTodoInputText = document.getElementById('addTodoInputText');
addTodoInputText.addEventListener('keyup', function(event){
	var inputText = addTodoInputText.value;
	if (event.keyCode == 13){
		handlers.addHandler(inputText);
	}
});


/* What is used to split data on a specific variable experiment in Microsoft intelligence suite?
 Stratified split 
 What inputs does the feature “train model” need in Microsoft intelligence suite machine learning? 
 Dataset and untrained model 
 What feature is used in  Microsoft intelligence suite machine learning to validate a trained model?
  Score model
  */ 